﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus.regular_expressions.Models
{
    public class ImageInfo
    {
        public string Source { get; }
        public string Format { get; }

        public ImageInfo(string source, string format)
        {
            Source = source;
            Format = format;
        }

        public override string ToString()
        {
            return Source;
        }
    }
}
