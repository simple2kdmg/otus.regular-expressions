﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using otus.regular_expressions.Interfaces;

namespace otus.regular_expressions.Models
{
    class HtmlParser : IHtmlParser
    {
        public async Task<string> GetHtmlTextAsync(string url)
        {
            using var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                return await httpClient.GetStringAsync(url);
            }

            throw new ArgumentException("Bad request.");
        }

        public async Task<List<ImageInfo>> GetImageSourcesAsync(string url)
        {
            var html = await GetHtmlTextAsync(url);
            var pattern = "<img[\\w\\s\\d=\"'-:\\/]*src=[\"'](https?://[\\w:\\/.?=]*(.png|.jpg))[\"'][\\w\\s\\d=\"'-:\\/]*>";
            
            return Regex.Matches(html, pattern)
                .Where(match => !String.IsNullOrWhiteSpace(match.Groups[1].Value) && !String.IsNullOrWhiteSpace(match.Groups[2].Value))
                .Select(match => new ImageInfo(match.Groups[1].Value, match.Groups[2].Value))
                .ToList();
        }
    }
}
