﻿using System;
using System.IO;
using System.Threading.Tasks;
using otus.regular_expressions.Interfaces;

namespace otus.regular_expressions.Models
{
    class FileManager : IFileManager
    {
        public async Task SaveFile(Stream stream, string directoryPath, string fileName)
        {
            CreateDirectory(directoryPath);
            var path = Path.Combine(directoryPath, fileName);
            await using var fs = new FileStream(path, FileMode.Create);
            await stream.CopyToAsync(fs);
        }
        private void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
    }
}
