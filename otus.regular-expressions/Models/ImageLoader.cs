﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using otus.regular_expressions.Interfaces;


namespace otus.regular_expressions.Models
{
    class ImageLoader : ILoader
    {
        private readonly IFileManager _fileManager;

        public ImageLoader(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }
        public async Task SaveImagesToFilesAsync(List<ImageInfo> infos, string folderPath)
        {
            foreach (var t in infos)
            {
                await SaveImageToFileAsync(t, $"img-{Guid.NewGuid()}", folderPath);
            }
        }

        public async Task<Stream> GetImageAsync(string source)
        {
            using HttpClient httpClient = new HttpClient();
            var response = await httpClient.GetAsync(source);
            return await response.Content.ReadAsStreamAsync();
        }

        private async Task SaveImageToFileAsync(ImageInfo info, string name, string folderPath)
        {
            var stream = await GetImageAsync(info.Source);
            await _fileManager.SaveFile(stream, folderPath, name + info.Format);
        }
    }
}
