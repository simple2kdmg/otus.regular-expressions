﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using otus.regular_expressions.Models;

namespace otus.regular_expressions.Interfaces
{
    public interface ILoader
    {
        Task SaveImagesToFilesAsync(List<ImageInfo> sources, string folderPath);
    }
}
