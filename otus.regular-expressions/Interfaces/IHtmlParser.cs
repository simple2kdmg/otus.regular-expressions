﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using otus.regular_expressions.Models;

namespace otus.regular_expressions.Interfaces
{
    public interface IHtmlParser
    {
        Task<string> GetHtmlTextAsync(string url);
        Task<List<ImageInfo>> GetImageSourcesAsync(string url);
    }
}
