﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace otus.regular_expressions.Interfaces
{
    public interface IFileManager
    {
        Task SaveFile(Stream fileStream, string folderPath, string fileName);
    }
}
