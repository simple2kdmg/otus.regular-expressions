﻿using System;
using System.Threading.Tasks;
using otus.regular_expressions.Models;

namespace otus.regular_expressions
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var htmlParser = new HtmlParser();
            var imgLoader = new ImageLoader(new FileManager());
            var infos = await htmlParser.GetImageSourcesAsync("https://habr.com/ru/company/macloud/blog/559902/");

            await imgLoader.SaveImagesToFilesAsync(infos, @"D:\temp");
        }
    }
}
